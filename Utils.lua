local GT = LibStub("AceAddon-3.0"):GetAddon("GoldTracker")
local L = LibStub("AceLocale-3.0"):GetLocale("GoldTracker")

local math_abs = math.abs
local math_ceil = math.ceil
local math_floor = math.floor

local GetMoney = GetMoney
local GetMoneyString = GetMoneyString
local GetQuestResetTime = GetQuestResetTime
local GetServerTime = GetServerTime
local SecondsToTime = SecondsToTime

local COLOR_GREEN = "|cFF00FF00"
local COLOR_RED = "|cFFFF0000"

local DAY = 86400

-- not sure if this is the best way to do it
-- depends on whether the time always comes from the login server or sometimes from the instance server
-- that could lead to inconsistent handling of day limits
function GT:GetTime()
	return GetServerTime()
end

function GT:GetMoneyMessage(money)
	return GetMoneyString(math_abs(money))
end

local function CalculateDifferenceOfDays(time1, time2)
	local diff = math_abs(time1 - time2)
	return math_ceil(diff / DAY), diff
end

function GT:CalculateDifferenceOfDays(init, faction, realm)
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	local diffDays, diffSeconds = CalculateDifferenceOfDays(self:GetResetTime(faction, realm), self:GetTime())
	if diffDays == 0 then -- edge case for when reset time is equal to GetServerTime()
		diffDays = 1
	end
	if init then
		self:InitializeNewDays(diffDays, faction, realm)
	end
	return diffDays, diffSeconds
end

function GT:CreateMoneyStrings(diffDays, diffSeconds)
	local color = ""
	local totalAmount = self:GetTotalDifference()
	local avgDailyAmount = math_floor(totalAmount / diffDays)
	local currentDailyAmount = self:GetTodayDifference()
	local strings = {}

	strings.resetInformation = SecondsToTime(diffSeconds)

	if self.sessionDifference >= 0 then
		color = COLOR_GREEN
	else
		color = COLOR_RED
	end
	strings.sessionTextValue = color .. self:GetMoneyMessage(self.sessionDifference) .. "|r"

	if currentDailyAmount >= 0 then
		color = COLOR_GREEN
	else
		color = COLOR_RED
	end
	strings.todayTextValue = color .. self:GetMoneyMessage(currentDailyAmount) .. "|r"

	if avgDailyAmount >= 0 then
		color = COLOR_GREEN
	else
		color = COLOR_RED
	end
	strings.dailyAvgTextValue = color .. self:GetMoneyMessage(avgDailyAmount) .. "|r"

	if totalAmount >= 0 then
		color = COLOR_GREEN
	else
		color = COLOR_RED
	end
	strings.totalTextValue = color .. self:GetMoneyMessage(totalAmount) .. "|r"
	return strings
end

function GT:SetFontStringsToFrame(frame, strings)
	frame.titleText:SetText(L["Last reset: "] .. strings.resetInformation)
	frame.sessionText.right:SetText(strings.sessionTextValue)
	frame.todayText.right:SetText(strings.todayTextValue)
	frame.dailyAvgText.right:SetText(strings.dailyAvgTextValue)
	frame.totalText.right:SetText(strings.totalTextValue)
end

function GT:SetFontStringsToTooltip(tooltip, strings)
	tooltip:AddLine(L["Last reset: "] .. strings.resetInformation)
	tooltip:AddDoubleLine(L["Session:"], strings.sessionTextValue, 1, 1, 1)
	tooltip:AddDoubleLine(L["Today:"], strings.todayTextValue, 1, 1, 1)
	tooltip:AddDoubleLine(L["DailyAvg:"], strings.dailyAvgTextValue, 1, 1, 1)
	tooltip:AddDoubleLine(L["Total:"], strings.totalTextValue, 1, 1, 1)
end

function GT:PLAYER_MONEY()
	local money = GetMoney()
	local change = money - self.playerMoney
	self.sessionDifference = self.sessionDifference + change
	local diffDays, diffSeconds = self:CalculateDifferenceOfDays(true)

	self:SetCharTodayDifference(self:GetCharTodayDifference() + change)
	self:SetCharTotalDifference(self:GetCharTotalDifference() + change)
	self.playerMoney = money

	if self.frame ~= nil then
		self:SetFontStringsToFrame(self.frame, self:CreateMoneyStrings(diffDays, diffSeconds))
	end
end

function GT:Reset()
	local resetTime
	if self:IsConstantResetTime() then
		local secondsToQuestReset = GetQuestResetTime()
		local secondsSinceLastQuestReset = DAY - secondsToQuestReset
		resetTime = self:GetTime() - secondsSinceLastQuestReset
	else
		resetTime = self:GetTime()
	end

	self:SetResetTime(resetTime)
	self.sessionDifference = 0
	self:ResetAllDifferences()
	self:PLAYER_MONEY()
	self:Print(L["resetted (total and session)."])
end
