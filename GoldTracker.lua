local GT = LibStub("AceAddon-3.0"):GetAddon("GoldTracker")
local L = LibStub("AceLocale-3.0"):GetLocale("GoldTracker")

local GetMoney = GetMoney
local GetMoneyString = GetMoneyString

function GT:OnEnable()
	self:RegisterEvent("PLAYER_MONEY")
	self.playerMoney = GetMoney()
	self.sessionDifference = 0
	self:Print(L["enabled with inital value "] .. GetMoneyString(self.playerMoney))
	self:CreateGUI()
end

function GT:HandleSlashCommands(input)
	local arg = input:trim()
	if arg == "show" then
		self:ShowMainFrame()
		self:SetFrameHidden(false)
	elseif arg == "hide" then
		self:HideMainFrame()
		self:SetFrameHidden(true)
	elseif arg == "reset" then
		self:ShowReset()
	end
end
