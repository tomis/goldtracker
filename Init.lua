local GT = LibStub("AceAddon-3.0"):NewAddon("GoldTracker", "AceConsole-3.0", "AceEvent-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale("GoldTracker")
local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")
local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local AceDB = LibStub("AceDB-3.0")

local GetRealmName = GetRealmName
local InterfaceOptionsFrame = InterfaceOptionsFrame
local UnitFactionGroup = UnitFactionGroup
local UnitName = UnitName

local defaults = {
	global = {
		factionrealm = {},
		framePositionX = 10,
		framePositionY = -35,
		frameHidden = false,
		isConstantResetTime = false
	}
}

local options = {
	order = 1,
	type = "group",
	name = "GoldTracker",
	childGroups = "select",
	args = {
		general = {
			inline = true,
			order = 1,
			name = L["General"],
			desc = L["General options for GoldTracker"],
			type = "group",
			args = {
				frameShown = {
					order = 1,
					name = L["Show main frame"],
					desc = L["Show or hide the main frame"],
					type = "toggle",
					width = "full",
					get = function() return not GT:IsFrameHidden() end,
					set = function(info, value)
						if GT:IsFrameHidden() then
							GT:ShowMainFrame()
							GT:SetFrameHidden(false)
						else
							GT:HideMainFrame()
							GT:SetFrameHidden(true)
						end
					end
				}
			}

		},
		reset = {
			inline = true,
			order = 1,
			name = L["Reset"],
			desc = L["Configures the reset behavior"],
			type = "group",
			args = {
				resettimeToggle = {
					order = 1,
					name = L["Constant reset time"],
					desc = L["Enables / disables the use of a constant reset time when a reset is issued (this will be the daily quest reset time)"],
					type = "toggle",
					width = "full",
					get = function() return GT:IsConstantResetTime() end,
					set = function() GT:SetIsConstantResetTime(not GT:IsConstantResetTime()) end
				},
				resetCommand = {
					order = 2,
					name = L["Reset"],
					desc = L["Resets all collected gold data to 0 and sets the new start time according to the constant reset time setting"],
					type = "execute",
					func = function() GT:ShowReset() end
				}
			}
		}
	}
}

function GT:ShowConfig()
	InterfaceOptionsFrame:Hide()
	AceConfigDialog:SetDefaultSize("GoldTracker", 680, 550)
	AceConfigDialog:Open("GoldTracker")
end

function GT:OnInitialize()
	AceConfigRegistry:RegisterOptionsTable("GoldTracker", options)
	AceConfigDialog:AddToBlizOptions("GoldTracker")
	self:RegisterChatCommand("goldtracker", "ShowConfig")
	self:RegisterChatCommand("gt", "ShowConfig")

	self.PLAYER_NAME = UnitName("player")
	self.REALM_NAME = GetRealmName("player")
	self.FACTION = UnitFactionGroup("player")

	self.db = AceDB:New("GoldTracker", defaults, true)
end

function GT:ShowMainFrame()
	if self.frame ~= nil then
		self.frame:Show()
		self:Print(L[" is now shown"])
	end
end

function GT:HideMainFrame()
	if self.frame ~= nil then
		self.frame:Hide()
		self:Print(L[" is now hidden"])
	end
end
