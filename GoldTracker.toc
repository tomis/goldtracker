## Interface: 80000
## Title: GoldTracker
## Notes: Keeps track of your gold.
## Author: Pasco of Shattrath-EU
## LastUpdate: 2018-07-08T02:35:36Z
## Version: 1.1.0

## X-Category: Inventory

## SavedVariables: GoldTracker

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

locales\GoldTracker-enUS.lua
locales\GoldTracker-deDE.lua

Init.lua
GoldTrackerDB.lua
Utils.lua

gui\ResetGUI.lua
gui\GraphGUI.lua
gui\MainGUI.lua

GoldTracker.lua

LDBGoldTracker.lua
