local L = LibStub("AceLocale-3.0"):NewLocale("GoldTracker", "enUS", true)

if not L then return end

L["resetted (total and session)."] = true
L["enabled with inital value "] = true
L[" day)"] = true
L[" days)"] = true
L["Session:"] = true
L["Today:"] = true
L["DailyAvg:"] = true
L["Total:"] = true
L["Reset"] = true
L["Graph"] = true
L["Options"] = true
L["Not implemented yet"] = true
L["Resetting GoldTracker"] = true
L["Do you wish to reset the data?"] = true
L["Yes"] = true
L["No"] = true
L[" is now shown"] = true
L[" is now hidden"] = true
L["Last reset: "] = true
L[" day ago"] = true
L[" days ago"] = true
L["Total"] = true

-- config option strings
L["General"] = true
L["General options for GoldTracker"] = true
L["Show main frame"] = true
L["Show or hide the main frame"] = true
L["Configures the reset behavior"] = true
L["Constant reset time"] = true
L["Enables / disables the use of a constant reset time when a reset is issued (this will be the daily quest reset time)"] = true
L["Resets all collected gold data to 0 and sets the new start time according to the constant reset time setting"] = true

