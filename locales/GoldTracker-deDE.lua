local L = LibStub("AceLocale-3.0"):NewLocale("GoldTracker", "deDE")

if not L then return end

L["resetted (total and session)."] = "Resettet (Gesamt und Sitzung)."
L["enabled with inital value "] = "aktiviert mit initialem Wert "
L[" day)"] = " Tag)"
L[" days)"] = " Tage)"
L["Session:"] = "Sitzung:"
L["Today:"] = "Heute:"
L["DailyAvg:"] = "Täglich Ø:"
L["Total:"] = "Gesamt:"
L["Reset"] = "Reset"
L["Graph"] = "Graph"
L["Options"] = "Optionen"
L["Not implemented yet"] = "Noch nicht implementiert"
L["Resetting GoldTracker"] = "GoldTracker zurücksetzen"
L["Do you wish to reset the data?"] = "Sollen die Daten zurückgesetzt werden?"
L["Yes"] = "Ja"
L["No"] = "Nein"
L[" is now shown"] = " wird nun angezeigt"
L[" is now hidden"] = " ist nun versteckt"
L["Last reset: "] = "Letzter Reset vor "
L[" day ago"] = " Tag"
L[" days ago"] = " Tagen"
L["Total"] = "Gesamt"

-- config option strings
L["General"] = "Allgemein"
L["General options for GoldTracker"] = "Allgemeine Einstellungen für GoldTracker"
L["Show main frame"] = "Zeige das Hauptfenster"
L["Show or hide the main frame"] = "Zeige oder verstecke das Hauptfenster"
L["Configures the reset behavior"] = "Einstellungen bezüglich des Reset-Verhaltens"
L["Constant reset time"] = "Konstante Reset-Zeit"
L["Enables / disables the use of a constant reset time when a reset is issued (this will be the daily quest reset time)"] =
	"Aktiviert / deaktiviert die Verwendung einer konstanten Reset-Zeit, wenn ein Reset durchgeführt wird (diese entspricht der Täglichen Quest Reset-Zeit)"
L["Resets all collected gold data to 0 and sets the new start time according to the constant reset time setting"] =
	"Setzt alle gesammelten Golddaten auf 0 zurück und wählt die neue Startzeit anhand der 'Konstante Reset-Zeit'-Einstellung"
