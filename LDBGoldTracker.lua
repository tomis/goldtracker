local GT = LibStub("AceAddon-3.0"):GetAddon("GoldTracker")
local LDB = LibStub:GetLibrary("LibDataBroker-1.1")

if not LDB then return end

GT.LDB = LDB:NewDataObject("GoldTracker_Broker", {
	type = "data source",
	icon = "Interface\\Icons\\INV_Misc_Coin_01",
	text = "GoldTracker"
})

function GT.LDB:OnTooltipShow()
	local diffDays, diffSeconds = GT:CalculateDifferenceOfDays()
	local moneyStrings = GT:CreateMoneyStrings(diffDays, diffSeconds)
	GT:SetFontStringsToTooltip(self, moneyStrings)
end

function GT.LDB:OnClick(button)
	if button == "LeftButton" then
		GT:ShowGraph()
	elseif button == "RightButton" then
		GT:ShowReset()
	end
end
