local GT = LibStub("AceAddon-3.0"):GetAddon("GoldTracker")
local L = LibStub("AceLocale-3.0"):GetLocale("GoldTracker")

local CreateFrame = CreateFrame
local UIParent = UIParent

function GT:CreateResetWarning()
	if self.resetFrame == nil then
		self.resetFrame = CreateFrame("Frame", nil, UIParent)
		local f = self.resetFrame

		f:SetPoint("CENTER", UIParent)
		f:SetHeight(84)
		f:SetWidth(266)
		f:EnableMouse(true)
		f:SetMovable(true)

		f:RegisterForDrag("LeftButton")
		f:SetScript("OnDragStart", f.StartMoving)
		f:SetScript("OnDragStop", f.StopMovingOrSizing)

		f:SetBackdrop({
			bgFile = "Interface\\Tooltips\\UI-Tooltip-Background", tile = true, tileSize = 16,
			edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 32,
			insets = { left = 1, right = 1, top = 1, bottom = 1 }})
		f:SetBackdropBorderColor(1.0, 0.0, 0.0)
		f:SetBackdropColor(24 / 255, 24 / 255, 24 / 255)

		f.title = f:CreateFontString(nil, "OVERLAY", "GameFontNormal")
		f.title:SetPoint("TOP", f, "TOP", 0, -8)
		f.title:SetTextColor(1.0, 1.0, 1.0, 1.0)
		f.title:SetText(L["Resetting GoldTracker"])

		f.text = f:CreateFontString(nil, "OVERLAY", "GameFontNormal")
		f.text:SetPoint("CENTER", f, "CENTER", 0, 5)
		f.text:SetTextColor(1.0, 1.0, 1.0, 1.0)
		f.text:SetText(L["Do you wish to reset the data?"])

		f.yesButton = CreateFrame("Button", nil, f, "OptionsButtonTemplate")
		f.yesButton:SetWidth(90)
		f.yesButton:SetHeight(20)
		f.yesButton:SetPoint("BOTTOMRIGHT", f, "BOTTOM", -4, 10)
		f.yesButton:SetText(L["Yes"])
		f.yesButton:SetScript("OnClick", function()
			GT:Reset()
			f:Hide()
		end)

		f.noButton = CreateFrame("Button", nil, f, "OptionsButtonTemplate")
		f.noButton:SetWidth(90)
		f.noButton:SetHeight(20)
		f.noButton:SetPoint("BOTTOMLEFT", f, "BOTTOM", 4, 10)
		f.noButton:SetText(L["No"])
		f.noButton:SetScript("OnClick", function()
			f:Hide()
		end)

		f:Hide()
		f:SetFrameStrata("DIALOG")
	end
end

function GT:ShowReset()
	if self.resetFrame == nil then
		self:CreateResetWarning()
	end

	self.resetFrame:Show()
end
