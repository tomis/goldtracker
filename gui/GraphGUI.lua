local GT = LibStub("AceAddon-3.0"):GetAddon("GoldTracker")
local L = LibStub("AceLocale-3.0"):GetLocale("GoldTracker")
local AceGUI = LibStub:GetLibrary("AceGUI-3.0")
local Graph = LibStub:GetLibrary("LibGraph-2.0-BarChart")

local math_abs = math.abs
local math_ceil = math.ceil
local math_floor = math.floor
local math_log10 = math.log10
local math_pow = math.pow
local str_find = string.find
local str_sub = string.sub
local pairs = pairs
local tinsert = table.insert

local CreateFrame = CreateFrame
local UIParent = UIParent

local TOTAL_KEY = L["Total"]

local currentFactionRealmKey

function GT:BuildFactionRealmDropdown()
	local list = {}
	for k, _ in pairs(self.db.global.factionrealm) do
		list[k] = k
	end
	return list
end

function GT:BuildCharacterDropdown(factionrealmKey)
	local list = {}
	local order = {}
	list[TOTAL_KEY] = TOTAL_KEY
	order[1] = TOTAL_KEY
	local count = 2

	for k, _ in pairs(self.db.global.factionrealm[factionrealmKey].char) do
		list[k] = k
		order[count] = k
		count = count + 1
	end
	return list, order
end

local function GetRoundedTickRange(range, tickCount)
	local unroundedTickSize = range / (tickCount - 1)
	local x = math_ceil(math_log10(unroundedTickSize) - 1)
	local pow10x = math_pow(10, x)
	return math_ceil(unroundedTickSize / pow10x) * pow10x;
end

function GT:UpdateGraphData(dataFunc, numberOfSteps, char, faction, realm)
	local data = {}
	local minY = 0
	local maxY = 100
	local minX = 0
	local maxX = numberOfSteps + 1

	for i = 1, maxX - 1 do
		data[i] = math_floor(dataFunc(self, i, char, faction, realm) / 10000) -- calculate gold from copper value

		if data[i] < minY then
			minY = data[i]
		elseif data[i] > maxY then
			maxY = data[i]
		end
	end

	-- adjust the Y-scale to not have the largest bars stick to the upper/lower frame border
	maxY = maxY + maxY / 10
	minY = minY + minY / 10

	local c = self.graphFrame.chart
	c:SetXAxis(minX, maxX)
	c:SetYAxis(minY, maxY)
	c:SetGridSpacing(1, GetRoundedTickRange(math_abs(minY - maxY), 10))

	c:ResetData()
	c:AddBarData(data, {0.8, 0, 0, 0.8})
end

local function split(s, delimiter)
	local results = {}
	local start = 1
	local splitStart, splitEnd = str_find(s, delimiter, start, true)
	while splitStart do
		tinsert(results, str_sub(s, start, splitStart - 1))
		start = splitEnd + 1
		splitStart, splitEnd = str_find(s, delimiter, start, true)
	end
	tinsert(results, str_sub(s, start))
	return results
end

function GT:CreateGraph()
	if self.graphFrame == nil then
		self.graphFrame = CreateFrame("Frame", nil, UIParent)
		local f = self.graphFrame

		f:SetPoint("CENTER", UIParent, "CENTER", 0, 100)
		f:SetHeight(650)
		f:SetWidth(800)
		f:EnableMouse(true)
		f:SetMovable(true)

		f:RegisterForDrag("LeftButton")
		f:SetScript("OnDragStart", f.StartMoving)
		f:SetScript("OnDragStop", f.StopMovingOrSizing)

		f:SetBackdrop({
			bgFile = "Interface\\Tooltips\\UI-Tooltip-Background", tile = true, tileSize = 16,
			edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 32,
			insets = { left = 1, right = 1, top = 1, bottom = 1 }})
		f:SetBackdropBorderColor(1.0, 0.0, 0.0)
		f:SetBackdropColor(24 / 255, 24 / 255, 24 / 255)
		f:SetFrameStrata("DIALOG")

		f.characterDropdown = AceGUI:Create("Dropdown")
		currentFactionRealmKey = self:concatKeys(self.FACTION, self.REALM_NAME)

		f.factionrealmDropdown = AceGUI:Create("Dropdown")
		f.factionrealmDropdown:SetParent(f)
		f.factionrealmDropdown:SetPoint("TOPLEFT", f, 40, -10)
		f.factionrealmDropdown.frame:SetFrameStrata("DIALOG")
		f.factionrealmDropdown:SetList(self:BuildFactionRealmDropdown())
		f.factionrealmDropdown:SetLabel("")
		f.factionrealmDropdown:SetText(currentFactionRealmKey)
		f.factionrealmDropdown:SetCallback("OnValueChanged", function(self, event, value)
			currentFactionRealmKey = value

			-- init the days for another realm (they are normally only initialized if there is a logon on said faction/realm)
			local factionrealm = split(value, " - ")
			GT:CalculateDifferenceOfDays(true, factionrealm[1], factionrealm[2])

			f.factionrealmDropdown:SetText(value)
			f.characterDropdown:SetList(GT:BuildCharacterDropdown(value))
			f.characterDropdown:Fire("OnValueChanged", TOTAL_KEY)
		end)

		f.characterDropdown:SetParent(f)
		f.characterDropdown:SetPoint("LEFT", f.factionrealmDropdown.frame, "RIGHT", 40, 0)
		f.characterDropdown.frame:SetFrameStrata("DIALOG")
		f.characterDropdown:SetList(self:BuildCharacterDropdown(currentFactionRealmKey))
		f.characterDropdown:SetLabel("")
		f.characterDropdown:SetText(TOTAL_KEY)
		f.characterDropdown:SetCallback("OnValueChanged", function(self, event, value)
			local factionrealm = split(currentFactionRealmKey, " - ")
			local faction = factionrealm[1]
			local realm = factionrealm[2]
			local char = value
			f.characterDropdown:SetText(value)
			if value == TOTAL_KEY then
				char = nil
			end
			GT:UpdateGraphData(GT.GetDayDifference, GT:GetNumberOfDays(faction, realm), char, faction, realm)
		end)

		f.closeButton = CreateFrame("Button", nil, f, "UIPanelCloseButton")
		f.closeButton:SetPoint("TOPRIGHT", f, "TOPRIGHT", 0, 0)
		f.closeButton:SetScript("OnClick", function()
			f:Hide()
			f.factionrealmDropdown.frame:Hide()
			f.characterDropdown.frame:Hide()
		end)

		-- name, parent, relative, relativeTo, offsetX, offsetY, width, height
		self.graphFrame.chart = Graph:CreateBarChart(nil, self.graphFrame, "CENTER", "CENTER", 0, 0, 740, 540)
		local c = self.graphFrame.chart
		c:SetGridColor({0.5, 0.5, 0.5, 0.5})
		c:SetAxisDrawing(true, true)
		c:SetAxisColor({1.0, 1.0, 1.0, 1.0})
		c:SetYLabels(true, false)
		c:SetXLabels(false, true)
	end
end

function GT:ShowGraph()
	if self.graphFrame == nil then
		self:CreateGraph()
	end

	self.graphFrame.characterDropdown:SetValue(TOTAL_KEY)
	self.graphFrame.characterDropdown:Fire("OnValueChanged", TOTAL_KEY)
	self.graphFrame:Show()
	self.graphFrame.factionrealmDropdown.frame:Show()
	self.graphFrame.characterDropdown.frame:Show()
end
