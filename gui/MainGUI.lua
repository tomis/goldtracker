local GT = LibStub("AceAddon-3.0"):GetAddon("GoldTracker")
local L = LibStub("AceLocale-3.0"):GetLocale("GoldTracker")
local SimpleLineFrame = LibStub:GetLibrary("LibSimpleLineFrame-1.0")

local CreateFrame = CreateFrame
local UIParent = UIParent

function GT:CreateGUI()
	if self.frame == nil then
		local diffDays, diffSeconds = self:CalculateDifferenceOfDays(true)

		self.frame = SimpleLineFrame:New("GoldTracker", {})
		local f = self.frame
		local x, y = self:GetFramePositionX() or 10, self:GetFramePositionY() or -35

		f:SetPoint("TOPLEFT", UIParent, "TOPLEFT", x, y)
		f:SetHeight(110)
		f:SetWidth(220)
		f:SetBackdrop({bgFile = "Interface/Tooltips/UI-Tooltip-Background",
		               edgeFile = "Interface/Tooltips/UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 16,
		               insets = { left = 4, right = 4, top = 4, bottom = 4 }})
		f:EnableMouse(true)
		f:SetMovable(true)
		f:SetFrameStrata("MEDIUM")

		f:RegisterForDrag("LeftButton")
		f:SetScript("OnDragStart", f.StartMoving)
		f:SetScript("OnDragStop", function()
			f:StopMovingOrSizing()
			GT:SaveFramePosition()
		end)

		f.closeButton = CreateFrame("Button", nil, f, "UIPanelCloseButton")
		f.closeButton:SetPoint("TOPRIGHT", f, "TOPRIGHT", 3, 3)
		f.closeButton:SetScript("OnClick", function()
			GT:HideMainFrame()
			GT:SetFrameHidden(true)
		end)

		f.titleText = f:CreateFontString(nil, "OVERLAY", "GameFontNormal")
		f.titleText:SetPoint("TOP", 0, 10)

		f.sessionText = f:AddLine(L["Session:"], "")
		f.todayText = f:AddLine(L["Today:"], "")
		f.dailyAvgText = f:AddLine(L["DailyAvg:"], "")
		f.totalText = f:AddLine(L["Total:"], "")

		self:SetFontStringsToFrame(f, self:CreateMoneyStrings(diffDays, diffSeconds))

		f.resetButton = CreateFrame("Button", nil, f, "OptionsButtonTemplate")
		f.resetButton:SetPoint("TOPLEFT", f.totalText, "BOTTOMLEFT", 0, -5)
		f.resetButton:SetWidth(60)
		f.resetButton:SetHeight(20)
		f.resetButton:SetText(L["Reset"])
		f.resetButton:EnableMouse(true)
		f.resetButton:SetScript("OnClick", function(self, button, down)
			GT:ShowReset()
		end)

		f.dataButton = CreateFrame("Button", nil, f, "OptionsButtonTemplate")
		f.dataButton:SetPoint("TOPLEFT", f.resetButton, "TOPRIGHT", 10, 0)
		f.dataButton:SetHeight(20)
		f.dataButton:SetWidth(60)
		f.dataButton:SetText(L["Graph"])
		f.dataButton:SetScript("OnClick", function(self, button, down)
			GT:ShowGraph()
		end)

		f.optionsButton = CreateFrame("Button", nil, f, "OptionsButtonTemplate")
		f.optionsButton:SetPoint("TOPLEFT", f.dataButton, "TOPRIGHT", 10, 0)
		f.optionsButton:SetHeight(20)
		f.optionsButton:SetWidth(60)
		f.optionsButton:SetText(L["Options"])
		f.optionsButton:SetScript("OnClick", function(self, button, down)
			GT:ShowConfig()
		end)

		if self:IsFrameHidden() then
			f:Hide()
		end
	end
end