--[[
This is a modified version of LibSimpleFrame-1.0 (Revision 53) by David Lynch (kemayo@gmail.com).
Website: http://www.wowace.com/wiki/LibSimpleFrame-1.0
SVN: http://svn.wowace.com/wowace/trunk/LibSimpleFrame-1.0/
License: LGPL v2.1
]]

local MAJOR_VERSION = "LibSimpleLineFrame-1.0"
local MINOR_VERSION = 90000 + tonumber(("$Revision: 1 $"):match("%d+")) or 0

-- #AUTODOC_NAMESPACE lib

local lib, oldMinor = LibStub:NewLibrary(MAJOR_VERSION, MINOR_VERSION)
if not lib then return end

local debugf = tekDebug and tekDebug:GetFrame("LibSimpleLineFrame-1.0")
local function Debug(...) if debugf then debugf:AddMessage(string.join(", ", tostringall(...))) end end

lib.registry = lib.registry or {}

local function Reset(self)
	self:Font()
	--self.left:SetFontObject(GameFontNormal)
	self.left:SetWidth(0)
	self.left:SetText('')
	self.left:SetJustifyH('LEFT')
	self.left:SetTextColor(GameFontNormal:GetTextColor())
	--self.right:SetFontObject(GameFontNormal)
	self.right:SetWidth(0)
	self.right:SetText('')
	self.right:SetJustifyH('RIGHT')
	self.right:SetTextColor(GameFontNormal:GetTextColor())
	self:SetWidth(self.core:GetWidth()-10)
	self:SetHeight(self.core.db.line_height or 12)
	self:Hide()
	return self
end

local function Color(self, lr, lg, lb, la, rr, rg, rb, ra)
	self.left:SetTextColor(lr or 1, lg or 1, lb or 1, la or 1)
	self.right:SetTextColor(rr or 1, rg or 1, rb or 1, ra or 1)
	return self
end

local function Font(self, font, size, flags)
	local default_font, default_size, default_flags = GameFontNormal:GetFont()
	self.left:SetFont(font or default_font, size or default_size, flags or default_flags)
	self.right:SetFont(font or default_font, size or default_size, flags or default_flags)
	return self
end

local function create_line(parent, i)
	local b = CreateFrame('Button', parent:GetName()..'_Line'..i, parent)
	b.core = parent

	local left = b:CreateFontString(b:GetName().."_Left", "OVERLAY")
	local right = b:CreateFontString(b:GetName().."_Right", "OVERLAY")
	--local texture = b:CreateTexture(b:GetName().."_Texture", "ARTWORK")
	
	left:SetPoint("TOPLEFT", b)
	right:SetPoint("TOPRIGHT", b, "TOPRIGHT", -5, 0)
	--texture:SetPoint("TOPRIGHT", b, "TOPLEFT")
	
	b.left = left
	b.right = right
	--b.texture = texture
	
	if i == 1 then
		b:SetPoint("TOPLEFT", parent, 7, -23)
	else
		b:SetPoint("TOPLEFT", parent.lines[i-1], "BOTTOMLEFT", 0, -3)
	end
	
	b.Reset = Reset
	b.Color = Color
	b.Font = Font

	if parent.db.lock then
		b:RegisterForDrag(nil)
		b:EnableMouse(false)
	else
		b:RegisterForDrag("LeftButton")
		b:EnableMouse(true)
	end

	return b:Reset()
end

local function AddLine(self, left, right, wrap, indent)
	indent = indent or 0
	self.current_line = (self.current_line or 0) + 1
	if not self.lines[self.current_line] then self.lines[self.current_line] = create_line(self, self.current_line) end
	local l = self.lines[self.current_line]
	l.left:SetPoint("TOPLEFT", l, "TOPLEFT", indent, 0)
	l.left:SetText(left)
	l.right:SetText(right)

	local _, left_size = l.left:GetFont()
	local _, right_size = l.right:GetFont()
	local biggest_size = max(left_size, right_size)
	if biggest_size > l:GetHeight() then
		l:SetHeight(biggest_size)
	end

	l.left:SetWidth(l:GetWidth() - l.right:GetWidth() - indent)
	if wrap then
		l:SetHeight(max(l.left:GetHeight(), l.right:GetHeight()))
	else
		l.left:SetHeight(l:GetHeight())
		l.right:SetHeight(l:GetHeight())
	end

	l:Show()
	return l
end

function lib:New(name, options, id, parent)
	--id will be passed to all handler functions if provided
	--parent will default to UIParent
	local frame
	if lib.registry[name] then
		frame = self.registry[name]
	else
		frame = CreateFrame("Frame", name, parent or UIParent)
		frame.lines = {}
		self.registry[name] = frame
	end
	frame.id = id
	frame.core = frame

	frame.AddLine = AddLine

	frame.db = options

	return frame
end

function lib:Get(name)
	return self.registry[name]
end
