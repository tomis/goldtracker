local GT = LibStub("AceAddon-3.0"):GetAddon("GoldTracker")

local pairs = pairs

function GT:concatKeys(key1, key2)
	return key1 .. " - " .. key2
end

function GT:GetCharTotalDifference(name, faction, realm)
	name = name or self.PLAYER_NAME
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	return self.db.global.factionrealm[self:concatKeys(faction, realm)].char[name].totalDifference
end

function GT:SetCharTotalDifference(value, name, faction, realm)
	name = name or self.PLAYER_NAME
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	self.db.global.factionrealm[self:concatKeys(faction, realm)].char[name].totalDifference = value
end

function GT:GetTotalDifference(faction, realm)
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME
	local value = 0

	for _, v in pairs(self.db.global.factionrealm[self:concatKeys(faction, realm)].char) do
		value = value + v.totalDifference
	end

	return value
end

function GT:GetCharDayDifference(index, name, faction, realm)
	name = name or self.PLAYER_NAME
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	return self.db.global.factionrealm[self:concatKeys(faction, realm)].char[name].dayDifferences[index]
end

function GT:GetDayDifference(index, name, faction, realm)
	if name ~= nil then
		return self:GetCharDayDifference(index, name, faction, realm)
	end

	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME
	local value = 0

	for _, v in pairs(self.db.global.factionrealm[self:concatKeys(faction, realm)].char) do
		value = value + (v.dayDifferences[index] or 0)
	end

	return value
end

function GT:GetNumberOfDays(faction, realm)
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	local charList = self.db.global.factionrealm[self:concatKeys(faction, realm)].char
	for _, v in pairs(charList) do
		return #v.dayDifferences
	end
end

function GT:GetCharTodayDifference(name, faction, realm)
	name = name or self.PLAYER_NAME
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	local dayDifferences = self.db.global.factionrealm[self:concatKeys(faction, realm)].char[name].dayDifferences
	return dayDifferences[#dayDifferences]
end

function GT:SetCharTodayDifference(value, name, faction, realm)
	name = name or self.PLAYER_NAME
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	local dayDifferences = self.db.global.factionrealm[self:concatKeys(faction, realm)].char[name].dayDifferences
	dayDifferences[#dayDifferences] = value
end

function GT:GetTodayDifference(faction, realm)
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	local key = self:concatKeys(faction, realm)
	local day = #self.db.global.factionrealm[key].char[self.PLAYER_NAME].dayDifferences

	return self:GetDayDifference(day, nil, faction, realm)
end

function GT:InitializeNewDays(totalDays, faction, realm)
	local key = self:concatKeys(faction or self.FACTION, realm or self.REALM_NAME)
	local factionrealm = self.db.global.factionrealm[key]

	for _, v in pairs(factionrealm.char) do
		for i = 1, totalDays do
			if v.dayDifferences[i] == nil then
				v.dayDifferences[i] = 0
			end
		end
	end
end

function GT:GetResetTime(faction, realm)
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	return self.db.global.factionrealm[self:concatKeys(faction, realm)].lastResetTime
end

function GT:SetResetTime(value, faction, realm)
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	self.db.global.factionrealm[self:concatKeys(faction, realm)].lastResetTime = value
end

function GT:GetFramePositionX()
	return self.db.global.framePositionX
end

function GT:SetFramePositionX(value)
	self.db.global.framePositionX = value
end

function GT:GetFramePositionY()
	return self.db.global.framePositionY
end

function GT:SetFramePositionY(value)
	self.db.global.framePositionY = value
end

function GT:SaveFramePosition()
	local f = self.frame
	local _, _, _, x, y = f:GetPoint()

	self:SetFramePositionX(x)
	self:SetFramePositionY(y)
end

function GT:IsFrameHidden()
	return self.db.global.frameHidden
end

function GT:SetFrameHidden(value)
	self.db.global.frameHidden = value
end

function GT:IsConstantResetTime()
	return self.db.global.isConstantResetTime
end

function GT:SetIsConstantResetTime(value)
	self.db.global.isConstantResetTime = value
end

function GT:ResetAllDifferences(faction, realm)
	faction = faction or self.FACTION
	realm = realm or self.REALM_NAME

	for _, v in pairs(self.db.global.factionrealm[self:concatKeys(faction, realm)].char) do
		v.totalDifference = 0
		v.dayDifferences = {0}
	end
end
